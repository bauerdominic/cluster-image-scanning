#!/bin/bash
# Install starboard and run the analyzer
# This installation should match https://aquasecurity.github.io/starboard/latest/operator/installation/kubectl/

VERSION="$1"
if [ -z "$VERSION" ]; then
  VERSION="$(curl -s https://api.github.com/repos/aquasecurity/starboard/releases/latest | jq -r .tag_name)"

  if [ "$VERSION" = null ]; then
    echo 'Could not get latest starboard release'
    echo "$VERSION" | jq -r .message
    exit 1
  fi

  echo "No version specified. Testing latest release (${VERSION})"
else
  echo "Testing with starboard ${VERSION}"
fi

manifests_path="https://raw.githubusercontent.com/aquasecurity/starboard/${VERSION}/deploy"

echo 'Installing starboard operator'

for manifest in \
  "${manifests_path}/crd/vulnerabilityreports.crd.yaml" \
  "${manifests_path}/crd/configauditreports.crd.yaml" \
  "${manifests_path}/crd/ciskubebenchreports.crd.yaml" \
  "${manifests_path}/static/01-starboard-operator.ns.yaml" \
  "${manifests_path}/static/02-starboard-operator.rbac.yaml" \
  "${manifests_path}/static/04-starboard-operator.deployment.yaml"
do
  kubectl apply -f "$manifest" || exit 1
done

echo -n 'Starboard operator image is: '
kubectl get deployment starboard-operator -n starboard-operator -o json | jq -r '.spec.template.spec.containers[] | select(.name == "operator") | .image'

echo 'Creating vulnerable deployment'
kubectl create deployment debian-test --image=debian:11

echo -n 'Waiting for resources to be scanned'
while true; do
  echo -n '.'
  [ -n "$(kubectl get vulnerabilityreports -o name)" ] && break
  sleep 1
done

status=0

echo
echo 'Running tests'
go clean -testcache && CIS_INTEGRATION_TEST=starboard go test -v ./... || status=1

echo 'Running analyzer'
go run . run || status=1

echo 'Cleaning up'
for manifest in \
  "${manifests_path}/static/04-starboard-operator.deployment.yaml" \
  "${manifests_path}/static/02-starboard-operator.rbac.yaml" \
  "${manifests_path}/static/01-starboard-operator.ns.yaml" \
  "${manifests_path}/crd/vulnerabilityreports.crd.yaml" \
  "${manifests_path}/crd/configauditreports.crd.yaml" \
  "${manifests_path}/crd/ciskubebenchreports.crd.yaml"
do
  kubectl delete -f "$manifest"
done

kubectl delete deployment debian-test

exit "$status"
