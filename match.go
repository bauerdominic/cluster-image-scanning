package main

import "os"

func match(path string, info os.FileInfo) (bool, error) {
	// This analyzer does not analyze the project files
	// It does not need to search for a file match, so we will always return true
	return true, nil
}
