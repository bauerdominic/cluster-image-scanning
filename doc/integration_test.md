# Integration Tests

Integration tests are used to ensure that the analyzer works correctly with
Kubernetes and Starboard. You can run them against any Kubernetes cluster.
For development, it is recommended to use a local cluster like
[k3d](https://k3d.io/#installation) or [minikube](https://minikube.sigs.k8s.io/docs/start/).

## Prerequisites

In order to run the integration tests, you will need:

1. A running kubernetes cluster
1. A kubeconfig file with the current context set to the cluster you want to test on
   (verify with `kubectl config current-context`)
1. Starboard installed on the cluster. This can be done with the
   [Starboard CLI](https://aquasecurity.github.io/starboard/v0.10.3/cli/getting-started/)
   using `starboard init`.

   You can verify if Starboard is installed using:

   ```shell
   $ kubectl api-resources --api-group aquasecurity.github.io
   NAME                   SHORTNAMES    APIGROUP                 NAMESPACED   KIND
   ciskubebenchreports    kubebench     aquasecurity.github.io   false        CISKubeBenchReport
   configauditreports     configaudit   aquasecurity.github.io   true         ConfigAuditReport
   kubehunterreports      kubehunter    aquasecurity.github.io   false        KubeHunterReport
   vulnerabilityreports   vulns,vuln    aquasecurity.github.io   true         VulnerabilityReport
   ```

## Running integration tests

Integration tests with only run if `CIS_INTEGRATION_TESTS` is set to one of these values:

- `all` - Run all integration tests
- `starboard` - Run only starboard integration tests

Integration tests can be run using `go test`. `go test` will try to cache test
results when the inputs are the same between runs. Since integration tests are
non-deterministic, you should clear the test cache before running them in order
to get accurate results.

```shell
go clean -testcache && CIS_INTEGRATION_TEST=all go test -v ./...
```
