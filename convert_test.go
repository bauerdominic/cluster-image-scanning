package main

import (
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

func TestReportConversion(t *testing.T) {
	input := `
[
  {
    "kind": "VulnerabilityReport",
    "apiVersion": "aquasecurity.github.io/v1alpha1",
    "metadata": {
      "name": "deployment-nginx-nginx",
      "namespace": "default",
      "uid": "08e86776-d51e-4c81-906d-17254ca34c6a",
      "resourceVersion": "1743",
      "generation": 1,
      "creationTimestamp": "2021-07-02T13:42:12Z",
      "labels": {
        "starboard.container.name": "nginx",
        "starboard.resource.kind": "Deployment",
        "starboard.resource.name": "nginx-deployment",
        "starboard.resource.namespace": "default"
      },
      "ownerReferences": [
        {
          "apiVersion": "apps/v1",
          "kind": "Deployment",
          "name": "nginx",
          "uid": "73ba1385-a99a-45a1-8727-96b210fe20fa",
          "controller": true,
          "blockOwnerDeletion": false
        }
      ],
      "managedFields": [
        {
          "manager": "starboard",
          "operation": "Update",
          "apiVersion": "aquasecurity.github.io/v1alpha1",
          "time": "2021-07-02T13:42:12Z",
          "fieldsType": "FieldsV1",
          "fieldsV1": {
            "f:metadata": {
              "f:labels": {
                ".": {},
                "f:starboard.container.name": {},
                "f:starboard.resource.kind": {},
                "f:starboard.resource.name": {},
                "f:starboard.resource.namespace": {}
              },
              "f:ownerReferences": {
                ".": {},
                "k:{\"uid\":\"73ba1385-a99a-45a1-8727-96b210fe20fa\"}": {
                  ".": {},
                  "f:apiVersion": {},
                  "f:blockOwnerDeletion": {},
                  "f:controller": {},
                  "f:kind": {},
                  "f:name": {},
                  "f:uid": {}
                }
              }
            },
            "f:report": {
              ".": {},
              "f:artifact": {
                ".": {},
                "f:repository": {},
                "f:tag": {}
              },
              "f:registry": {
                ".": {},
                "f:server": {}
              },
              "f:scanner": {
                ".": {},
                "f:name": {},
                "f:vendor": {},
                "f:version": {}
              },
              "f:summary": {
                ".": {},
                "f:criticalCount": {},
                "f:highCount": {},
                "f:lowCount": {},
                "f:mediumCount": {},
                "f:unknownCount": {}
              },
              "f:vulnerabilities": {}
            }
          }
        }
      ]
    },
    "report": {
      "updateTimestamp": null,
      "scanner": {
        "name": "Trivy",
        "vendor": "Aqua Security",
        "version": "0.16.0"
      },
      "registry": {
        "server": "index.docker.io"
      },
      "artifact": {
        "repository": "library/nginx",
        "tag": "1.16"
      },
      "summary": {
        "criticalCount": 0,
        "highCount": 0,
        "mediumCount": 2,
        "lowCount": 0,
        "noneCount": 0,
        "unknownCount": 0
      },
      "vulnerabilities": [
        {
          "vulnerabilityID": "CVE-2020-27350",
          "resource": "apt",
          "installedVersion": "1.8.2",
          "fixedVersion": "1.8.2.2",
          "severity": "MEDIUM",
          "title": "apt: integer overflows and underflows while parsing .deb packages",
          "primaryLink": "https://avd.aquasec.com/nvd/cve-2020-27350",
          "links": [],
          "score": 5.7
        },
        {
          "vulnerabilityID": "CVE-2020-3810",
          "resource": "apt",
          "installedVersion": "1.8.2",
          "fixedVersion": "1.8.2.1",
          "severity": "MEDIUM",
          "title": "",
          "primaryLink": "https://avd.aquasec.com/nvd/cve-2020-3810",
          "links": [],
          "score": 5.5
        }
      ]
    }
  }
]
`

	os.Setenv("CIS_CLUSTER_IDENTIFIER", "123")
	os.Setenv("CIS_CLUSTER_AGENT_IDENTIFIER", "46357")

	defer func() {
		os.Unsetenv("CIS_CLUSTER_IDENTIFIER")
		os.Unsetenv("CIS_CLUSTER_AGENT_IDENTIFIER")
	}()

	reader := strings.NewReader(input)
	expected := &report.Report{
		Version:  report.CurrentVersion(),
		Analyzer: metadata.AnalyzerName,
		Vulnerabilities: []report.Vulnerability{
			{
				Category:    metadata.Type,
				Name:        "CVE-2020-27350",
				Message:     "CVE-2020-27350 in apt",
				Description: "apt: integer overflows and underflows while parsing .deb packages",
				Severity:    report.SeverityLevelMedium,
				Confidence:  report.ConfidenceLevelUnknown,
				Solution:    "Upgrade apt from 1.8.2 to 1.8.2.2",
				Scanner:     metadata.IssueScanner,
				Location: report.Location{
					Dependency: &report.Dependency{
						Package: report.Package{Name: "apt"},
						Version: "1.8.2",
					},
					Image: "index.docker.io/library/nginx:1.16",
					KubernetesResource: &report.KubernetesResource{
						Namespace:     "default",
						Name:          "nginx-deployment",
						Kind:          "Deployment",
						ContainerName: "nginx",
						ClusterID:     "123",
						AgentID:       "46357",
					},
				},
				Identifiers: []report.Identifier{
					{
						Type:  report.IdentifierTypeCVE,
						Name:  "CVE-2020-27350",
						Value: "CVE-2020-27350",
						URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-27350",
					},
				},
				Links: []report.Link{{URL: "https://avd.aquasec.com/nvd/cve-2020-27350"}},
			},
			{
				Category:    metadata.Type,
				Name:        "CVE-2020-3810",
				Message:     "CVE-2020-3810 in apt",
				Description: "",
				Severity:    report.SeverityLevelMedium,
				Confidence:  report.ConfidenceLevelUnknown,
				Solution:    "Upgrade apt from 1.8.2 to 1.8.2.1",
				Scanner:     metadata.IssueScanner,
				Location: report.Location{
					Dependency: &report.Dependency{
						Package: report.Package{Name: "apt"},
						Version: "1.8.2",
					},
					Image: "index.docker.io/library/nginx:1.16",
					KubernetesResource: &report.KubernetesResource{
						Namespace:     "default",
						Name:          "nginx-deployment",
						Kind:          "Deployment",
						ContainerName: "nginx",
						ClusterID:     "123",
						AgentID:       "46357",
					},
				},
				Identifiers: []report.Identifier{
					{
						Type:  report.IdentifierTypeCVE,
						Name:  "CVE-2020-3810",
						Value: "CVE-2020-3810",
						URL:   "https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2020-3810",
					},
				},
				Links: []report.Link{{URL: "https://avd.aquasec.com/nvd/cve-2020-3810"}},
			},
		},
	}

	actual, err := convert(reader, ".")
	if err != nil {
		t.Fatalf("unexpected error while converting report: %s", err.Error())
	}

	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Unexpected conversion result. Expected:\n%#v\nbut got:\n%#v", expected, actual)
	}
}
