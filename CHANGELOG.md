# Cluster Image Scanning analyzer changelog

## v0.5.0
- Accept comma-separated resource filter values (!22)

## v0.4.5
- Add `agentID` to `KubernetesResource` in the vulnerability location data (!25)

## v0.4.4
- Upgrade Starboard to v0.13.1 (!24)
- Uses report schema version v14.0.4

## v0.4.3
- Upgrade Starboard to v0.13.0 (!21)

## v0.4.2
- Upgrade Go to v1.17 (!19)

## v0.4.1
- Disable report optimization

## v0.4.0
- Add `KubernetesResource` to the vulnerability location data (!12)

## v0.3.2
- Upgrade Starboard to v0.12.0 (!10)

## v0.3.1
- Fix a bug where rulesets would run with no ruleset file configured (!3)

## v0.3.0
- Rewrite prototype in Golang (!1, !2)
