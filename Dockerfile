
FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/build

COPY . .

RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
  go build -o /analyzer \
  -ldflags "-X '$(go list -m)/metadata.AnalyzerVersion=${CHANGELOG_VERSION}'"

FROM alpine:3.14

COPY --from=build /analyzer /analyzer

ENTRYPOINT []
CMD ["/analyzer", "run"]
