package metadata

import (
	"fmt"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v3"
)

const (
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "GitLab"

	// AnalyzerID identifies the analyzer
	AnalyzerID = "cluster-image-scanning"

	// AnalyzerName is the name of the analyzer
	AnalyzerName = AnalyzerID

	// Type identifies the security scanning Category of this analyzer
	Type = report.CategoryClusterImageScanning

	scannerID     = "starboard_trivy"
	scannerName   = "Trivy (via Starboard Operator)"
	scannerVendor = AnalyzerVendor
	scannerURL    = "https://github.com/aquasecurity/trivy"
)

var (
	// AnalyzerVersion is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time
	AnalyzerVersion = "not-configured"

	// AnalyzerUsage provides a one line usage string for the analyzer
	AnalyzerUsage = fmt.Sprintf("%s %s analyzer v%s", AnalyzerVendor, AnalyzerName, AnalyzerVersion)

	// IssueScanner describes the scanner used to find a vulnerability
	IssueScanner = report.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	// ReportScanner returns identifying information about a security scanner
	ReportScanner = report.ScannerDetails{
		ID:   scannerID,
		Name: scannerName,
		Vendor: report.Vendor{
			Name: scannerVendor,
		},
		URL: scannerURL,
	}
)
