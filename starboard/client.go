package starboard

import (
	"context"
	"fmt"
	"path"

	"github.com/aquasecurity/starboard/pkg/apis/aquasecurity/v1alpha1"
	"github.com/aquasecurity/starboard/pkg/kube"
	"github.com/aquasecurity/starboard/pkg/starboard"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// Client is used to retrieve objects from Starboard
type Client struct {
	// Kube is the Kubernetes client
	Kube client.Client
}

// NewClient creates a new Starboard client using the given kubeconfig
func NewClient(kubeconfig string) (*Client, error) {
	if kubeconfig == "" {
		log.Info("KUBECONFIG not set, defaulting to ~/.kube/config")
		home, err := homedir.Dir()
		if err != nil {
			return nil, fmt.Errorf("could not find home directory: %w", err)
		}
		kubeconfig = path.Join(home, ".kube", "config")
	}

	log.Infof("Using kubeconfig: %s", kubeconfig)
	// use the current context in kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		return nil, fmt.Errorf("error reading kubeconfig: %w", err)
	}

	scheme := starboard.NewScheme()
	k, err := client.New(config, client.Options{Scheme: scheme})
	if err != nil {
		return nil, fmt.Errorf("error creating kubernetes client: %w", err)
	}
	log.Infof("Using API server %s as Kubernetes control plane", config.Host)

	return &Client{
		Kube: k,
	}, nil
}

// GetVulnerabilityReports returns a list of vulnerability reports which match the
// objects with the given FilterArguments
func (c *Client) GetVulnerabilityReports(ctx context.Context, filterArgs *FilterArguments) ([]v1alpha1.VulnerabilityReport, error) {
	gvrs := make([]string, len(filterArgs.Kinds))
	for i, kind := range filterArgs.Kinds {
		gvr, err := kindToGVR(c, kind)
		if err != nil {
			return nil, err
		}
		gvrs[i] = gvr
	}
	filterArgs.Kinds = gvrs

	filter, err := NewFilter(filterArgs)
	if err != nil {
		return nil, fmt.Errorf("error initializing filters: %w", err)
	}
	matchingLabelsSelector := &client.MatchingLabelsSelector{
		Selector: filter.Selector(),
	}
	if !filter.Empty() {
		log.Infof("Label selectors: %s", matchingLabelsSelector.Selector)
	}
	var list v1alpha1.VulnerabilityReportList
	err = c.Kube.List(ctx, &list, matchingLabelsSelector)
	if err != nil {
		return nil, err
	}
	return list.Items, nil
}

// kindToGVR translates kinds into their true name. e.g. daemonset -> DaemonSet
func kindToGVR(client *Client, kind string) (string, error) {
	_, gvk, err := kube.GVRForResource(client.Kube.RESTMapper(), kind)
	if err != nil {
		log.Errorf("The CIS_RESOURCE_KIND given (%q) may be invalid", kind)
		return kind, fmt.Errorf("error getting GroupVersionKind for kind %q: %w", kind, err)
	}
	return gvk.Kind, nil
}
