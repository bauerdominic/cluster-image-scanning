package starboard

import (
	"github.com/aquasecurity/starboard/pkg/starboard"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/selection"
)

// FilterArguments contains resources filters.
type FilterArguments struct {
	Kinds          []string
	Namespaces     []string
	ResourceNames  []string
	ContainerNames []string
}

// Filter is used to limit retrieved vulnerability reports to the specified resources
type Filter struct {
	Requirements labels.Requirements
}

// NewFilter returns a new Filter.
func NewFilter(args *FilterArguments) (*Filter, error) {
	var (
		filter Filter
		err    error
	)

	err = filter.require(starboard.LabelResourceKind, args.Kinds)
	if err != nil {
		return nil, err
	}

	err = filter.require(starboard.LabelResourceNamespace, args.Namespaces)
	if err != nil {
		return nil, err
	}

	err = filter.require(starboard.LabelResourceName, args.ResourceNames)
	if err != nil {
		return nil, err
	}

	err = filter.require(starboard.LabelContainerName, args.ContainerNames)
	if err != nil {
		return nil, err
	}

	return &filter, nil
}

func (f *Filter) require(key string, vals []string) error {
	if len(vals) == 0 {
		return nil
	}

	req, err := labels.NewRequirement(key, selection.In, vals)
	if err != nil {
		return err
	}
	f.Requirements = append(f.Requirements, *req)

	return nil
}

// Empty returns true without any resource filters.
func (f Filter) Empty() bool {
	return len(f.Requirements) == 0
}

// Selector returns a label selector containing all resource filters.
func (f Filter) Selector() labels.Selector {
	return labels.NewSelector().Add(f.Requirements...)
}
