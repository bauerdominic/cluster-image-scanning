package starboard_test

import (
	"os"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/cluster-image-scanning/starboard"
)

func integrationTestsShouldRun() bool {
	integrationTest := os.Getenv("CIS_INTEGRATION_TEST")
	return strings.EqualFold(integrationTest, "starboard") || strings.EqualFold(integrationTest, "all")
}

func TestNewClient(t *testing.T) {
	if !integrationTestsShouldRun() {
		t.SkipNow()
	}

	kubeconfig := os.Getenv("CIS_KUBECONFIG")
	if kubeconfig == "" {
		kubeconfig = os.Getenv("KUBECONFIG")
	}

	_, err := starboard.NewClient(kubeconfig)
	if err != nil {
		t.Fatalf("could not create client: %s", err.Error())
	}
}
