# Cluster Image Scanning

### NOTE: This project is in an early stage of development, it is meant to be a guide for further analyzer development.

This analyzer is a Shell and Ruby script that connects to the Kubernetes cluster, fetches the vulnerability reports from the cluster, and create reports that are parseable by GitLab. This project also depends on [Security report schemas](https://gitlab.com/gitlab-org/security-products/security-report-schemas).

The resulting analyzer docker image is used with the [official template](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/#configuration).

## Current Settings

You can configure container scanning by using the [available CI/CD variables](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/#available-cicd-variables):

Please do not use Kubernetes-specific variables that are not documented as being supported by the GitLab analyzer.

## Release

To release a new version:
1. Create a tag matching `VERSION`
1. Create a release matching `VERSION`

If the version of the [`gitlab-org/security-products/analyzers/report`](https://gitlab.com/gitlab-org/security-products/analyzers/report)
dependency has changed, update the changelog with the current report schema version.

### Available image tags

- `edge`: HEAD of the default branch
- `latest`: latest tag build
- `MAJOR.MINOR.PATCH`: latest tag/schedule build matching the given version
- `MAJOR`: latest tag/schedule build matching the given major version number

## License

See the [LICENSE](LICENSE) file for more details.

## Contributing

Contributions are welcome, see the [CONTRIBUTING.md](CONTRIBUTING.md) for more details.

## Development Docs

Please see the [`doc/`](doc/) directory for additional documentation.
